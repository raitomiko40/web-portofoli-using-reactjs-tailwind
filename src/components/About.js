import React from 'react';
//countup
import CountUp from 'react-countup';
//intersection observer
import {useInView} from 'react-intersection-observer'
//motion
import {motion} from 'framer-motion'
//variant
import { fadeIn } from '../variants';


const About = () => {
  const[ref, inView] = useInView({
    threshold: 0.5,
  })
  return (
    <section 
            className='section section-about' 
            id='about'
            ref={ref}
            >
            <div className='container mx-auto '>
              <div className='flex flex-col  lg:flex-row lg:items-center lg:gap-x-20  h-screen '>
                <motion.div 
                  variants={fadeIn('up', 0.3)}
                  initial="hidden" whileInView={'show'}
                  viewport={{once:false, amount:0.7}}
                  className='flex-1 bg-about bg-contain bg-no-repeat h-[640px] mix-blend-lighten bg-top '>
                </motion.div>
                <div className='flex-1 '>
                <motion.h2 
                    variants={fadeIn('left', 0.3)}
                    initial="hidden" whileInView={'show'}
                    viewport={{once:false, amount:0.7}}
                    className='h2 text-accent '>
                    ABOUT ME<span></span>
                  </motion.h2>
                  <motion.h3 
                    variants={fadeIn('left', 0.3)}
                    initial="hidden" whileInView={'show'}
                    viewport={{once:false, amount:0.7}}
                    className='mb-4 text-justify'>
                    Saya seorang Web Developer dengan pengalaman lebih dari 1 tahun, <span> yang telah mengikuti course Fullstack Web Developer dari Bootcamp Binar Academy. </span>
                  </motion.h3>
                  {/* stats */}
                  <motion.div                  
                    variants={fadeIn('left', 0.3)}
                    initial="hidden" whileInView={'show'}
                    viewport={{once:false, amount:0.7}}                 
                    className='flex gap-x-6 lg:gap-x-10 mb-12'>
                    <div>
                      <div className='text-[40px] font-tertiary text-gradient mb-2'>
                        {
                          inView ?
                          <CountUp start={0} end={2} duration={3} />
                          :null
                        }
                      </div>
                      <div className='font=primary text-sm tracking-[2px]'>
                        Years of <br/>
                        Experience
                      </div>
                    </div>
                    <div>
                      <div className='text-[40px] font-tertiary text-gradient mb-2'>
                        {
                          inView ?
                          <CountUp start={0} end={20} duration={3} />
                          :null
                        } +
                      </div>
                      <div className='font=primary text-sm tracking-[2px]'>
                        Project <br/>
                        Completed
                      </div>
                    </div>
                  </motion.div>
                  <motion.div 
                      variants={fadeIn('left', 0.3)}
                      initial="hidden" whileInView={'show'}
                      viewport={{once:false, amount:0.7}}
                      className='flex max-w-max gap-x-6 items-center mb-12 mx-auto lg:mx-0'>
                      <button className='btn btn-lg'>Contact me</button>
                      <a href='#' className='text-gradient btn-link'>
                        My Portofolio
                      </a>
                    </motion.div>
                </div>
              </div>
            </div>
        </section>
  )
         
};

export default About;
