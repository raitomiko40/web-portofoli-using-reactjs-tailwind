import React from 'react';
//icon
import {BsArrowUpRight} from 'react-icons/bs'
//motion
import {motion} from 'framer-motion'
import { fadeIn } from '../variants';
import { Link } from 'react-scroll';

//services Data
const services = [
  {
    name: 'Web Developer',
    description:
          'lorem abad ggrgrg dsdasdwd dsdsadwdaw dsadawdwdwa dsdawdwada dadwadwdwa dwadawdwadwad dawds',
    link: 'Learn more',
  },
  {
    name: 'UI/UX Design',
    description:
          'lorem abad ggrgrg dsdasdwd dsdsadwdaw dsadawdwdwa dsdawdwada dadwadwdwa dwadawdwadwad dawds',
    link: 'Learn more',
  },
  {
    name: 'Design Grafis',
    description:
          'lorem abad ggrgrg dsdasdwd dsdsadwdaw dsadawdwdwa dsdawdwada dadwadwdwa dwadawdwadwad dawds',
    link: 'Learn more',
  },
  {
    name: 'Youtuber',
    description:
          'lorem abad ggrgrg dsdasdwd dsdsadwdaw dsadawdwdwa dsdawdwada dadwadwdwa dwadawdwadwad dawds',
    link: 'Learn more',
  },
  
]

const Services = () => {
  return (
          <section className='section' id='services'>
            <div className='container mx-auto'>
              <div className='flex flex-col lg:flex-row'>
                {/* tetx & image */}
                <motion.div
                  variants={fadeIn('right', 0.3)}
                  initial="hidden" whileInView={'show'}
                  viewport={{once:false, amount:0.7}}
                  className='flex-1 lg:bg-services lg:bg-bottom   bg-no-repeat mix-blend-lighten  lg:mb-0'>
                  <h2 className='h2 text-accent mb-6'>
                    What I Do.
                  </h2>
                  <h3 className='h3  max-w-[445px] mb-10 lg:mb-16 text-justify'>
                    I'm a Frelancer Front-end Developer with over 1 years of experience.
                  </h3>
                  <button className='btn btn-sm'> See my work</button>
                </motion.div>
                {/* services */}
                <div>
                  {/* services list */}
                  <motion.div 
                    variants={fadeIn('left', 0.3)}
                    initial="hidden" whileInView={'show'}
                    viewport={{once:false, amount:0.7}}
                    className='flex-1 '>
                    {services.map((services, index) => {
                      // desctructure service
                      const {name, description, link} = services
                      return(
                        <div 
                            className='border-b border-white/20 h-[146px] mb-[38px] flex ' 
                            key={index}>
                          <div className='max-w-[470px]'>
                            <h4 className='text-[20px] tracking-wider font-primary font-semibold mb-6'>{name}</h4>
                            <p className='font-secondary leading-tight'>
                              {description}
                              </p>
                          </div>
                          <div className='flex flex-col flex-1 items-end'>
                            <a href='#' className='btn w-9 h-9 mb-[42px] flex justify-center items-center'>
                              <BsArrowUpRight />
                            </a>
                            <a href='#' className='text-gradient text-sm'>{link}</a>
                          </div>
                        </div>
                      )
                    })}
                  </motion.div>
                </div>
              </div>
            </div>
          </section>
  )
};

export default Services;
