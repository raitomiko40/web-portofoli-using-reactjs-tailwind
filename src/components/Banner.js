import React from 'react';
//Image
import Image from '../assets/banner.svg'

//icon
import {FaGithub, FaGitlab, FaInstagram} from 'react-icons/fa'
//type animation
import {TypeAnimation} from 'react-type-animation'
//motion
import {motion} from 'framer-motion'
//variant
import { fadeIn } from '../variants'

const Banner = () => {
  return (
          <section className='min-h-[85vh] lg:min-h-[78vh] flex items-center' id='home'>
            <div className='container mx-auto'>
              <div className='flex flex-col gap-y-8 lg:flex-row lg:items-center lg:gap-x-12 '>
                <div className='flex-1 text-center font-secondary lg:text-left'>
                  <motion.h1 
                    variants={fadeIn('up', 0.3)}
                    initial="hidden" whileInView={'show'}
                    viewport={{once:false, amount:0.7}}
                    className=' font-bold leading-[0.8] text-[60px] lg:text-[110px]'>
                    SAYA<span></span>
                  </motion.h1>
                  <motion.div 
                    variants={fadeIn('up', 0.3)}
                    initial="hidden" whileInView={'show'}
                    viewport={{once:false, amount:0.7}}
                    className='mb-6 text-[28px] lg:text-[56px] font-secondary font-semibold uppercase leading-[1] '>
                    <span className='text-white mr-4 ' >seorang</span>
                    <TypeAnimation
                      sequence={[
                        'Web Developer',
                        1500,
                        'UI/UX Design',
                        1500,
                        'Design Graphic',
                        1500,
                        'Youtuber',
                        1500,
                      ]}
                      speed={50}
                      className='text-accent'
                      wrapper='span'
                      repeat={Infinity}
                    />
                  </motion.div>
                    <motion.p
                      variants={fadeIn('up', 0.3)}
                      initial="hidden" whileInView={'show'}
                      viewport={{once:false, amount:0.7}}
                      className='mb-8 max-w-lg max-auto lg:mx-0 text-justify'  >
                      Saya lulusan Program Studi Teknik Informatika (S1) dari Universitas Muslim Indonesia Makassar.
                      Memiliki komunikasi yang baik, disiplin tinggi, pekerja keras, dan mampu bekerja di bawah tekanan.
                    </motion.p>
                    <motion.div 
                      variants={fadeIn('up', 0.3)}
                      initial="hidden" whileInView={'show'}
                      viewport={{once:false, amount:0.7}}
                      className='flex max-w-max gap-x-6 items-center mb-12 mx-auto lg:mx-0'>
                      <button className='btn btn-lg'>Contact me</button>
                      <a href='#' className='text-gradient btn-link'>
                        My Portofolio
                      </a>
                    </motion.div>
                    <motion.div 
                      variants={fadeIn('up', 0.3)}
                      initial="hidden" whileInView={'show'}
                      viewport={{once:false, amount:0.7}}
                      className='flex text-[20px] gap-x-6 max-w-max mx-auto lg:mx-0'>
                      <a href='#'>
                        <FaGithub />
                      </a>
                      <a href='#'>
                        <FaGitlab />
                      </a>
                      <a href='#'>
                        <FaInstagram />
                      </a>
                    </motion.div>
                </div>
                <motion.div 
                  variants={fadeIn('down', 0.5)}
                  initial="hidden" whileInView={'show'}
                  
                  className='hidden lg:flex flex-1 max-w-[320px] lg:max-w-[482px]'>
                  <img src={Image} alt=''/>
                </motion.div>
              </div>
            </div>
          </section>
  )
};

export default Banner;
